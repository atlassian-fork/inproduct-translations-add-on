package com.atlassian.translations.inproduct.components;

import java.util.List;
import java.util.Map;

public class TranslationsRequest {

    private final String language;
    private final List<Product> products;
    private final Map<String, String> translations;

    public TranslationsRequest(String language, List<Product> products, Map<String, String> translations) {
        this.language = language;
        this.products = products;
        this.translations = translations;
    }

    public String getLanguage() {
        return language;
    }

    public List<Product> getProducts() {
        return products;
    }

    public Map<String, String> getTranslations() {
        return translations;
    }



    public static class Product {
        private final String name;
        private final String version;

        public String getName() {
            return name;
        }

        public String getVersion() {
            return version;
        }

        public Product(String name, String version) {
            this.name = name;
            this.version = version;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Product product = (Product) o;

            if (name != null ? !name.equals(product.name) : product.name != null) return false;
            return version != null ? version.equals(product.version) : product.version == null;

        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (version != null ? version.hashCode() : 0);
            return result;
        }
    }
}
