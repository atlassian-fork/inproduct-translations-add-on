package com.atlassian.translations.inproduct;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.Set;
import java.util.TreeSet;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.0
 */
public class TranslationValidator {

	/**
	 *
	 * @param original
	 * @param translation
	 * @return empty string if no error found or error description if found
	 */
	// todo throw exception on error instead of returning empty string?
	public static String validate(String original, String translation) {

// original = "Will check if the user with user name available as a ''{0}{1}{2}'' parameter in the context has the {0}{3}{2} permission.
// If the user name is null, the check is done against a null user.";
// translation = " ''{0}{1}{2}'' parameter in the  {0}{3}{2} permission. {3}.";
		 try {
			// todo unit test it
			MessageFormat originalFormat = new MessageFormat(original);
			MessageFormat translationFormat = new MessageFormat(translation);

			Field fieldMaxOffset = MessageFormat.class.getDeclaredField("maxOffset");
			Field fieldArgumentNumbers = MessageFormat.class.getDeclaredField("argumentNumbers");

			fieldMaxOffset.setAccessible(true);
			fieldArgumentNumbers.setAccessible(true);

			Integer originalMaxOffset = (Integer) fieldMaxOffset.get(originalFormat);
			Integer translationMaxOffset = (Integer) fieldMaxOffset.get(translationFormat);

			int[] originalArguments = (int[]) fieldArgumentNumbers.get(originalFormat);
			int[] translationArguments = (int[]) fieldArgumentNumbers.get(translationFormat);

			Set<Integer> originalArgumentsSet = new TreeSet<Integer>();
			Set<Integer> translationArgumentsSet = new TreeSet<Integer>();

			for (int i = 0; i <= originalMaxOffset; ++i) {
				originalArgumentsSet.add(originalArguments[i]);
			}

			for (int i = 0; i <= translationMaxOffset; ++i) {
				translationArgumentsSet.add(translationArguments[i]);
			}

			if (originalArgumentsSet.size() == translationArgumentsSet.size() && originalArgumentsSet.containsAll(translationArgumentsSet)) {
				// everything is OK
				return "";
			}

			return "Argument not found in original message";

		} catch (NoSuchFieldException e) {
			 return e.getMessage();
		} catch (IllegalAccessException e) {
			 return e.getMessage();
		} catch (IllegalArgumentException e) {
			return e.getMessage();
		}
	}
}
