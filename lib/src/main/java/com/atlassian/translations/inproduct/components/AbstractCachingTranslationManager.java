package com.atlassian.translations.inproduct.components;

import com.atlassian.translations.inproduct.MessageTransformUtil;
import com.atlassian.translations.inproduct.ao.AbstractAoTranslationsManager;
import com.atlassian.translations.inproduct.ao.entity.TranslationsEntity;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * User: kalamon
 * Date: 20.06.13
 * Time: 15:11
 */
public abstract class AbstractCachingTranslationManager implements TranslationManager {
    private volatile boolean cacheLoadStarted;
    private volatile boolean cacheInitialized;

    private volatile int initCounter = 0;

    private AbstractAoTranslationsManager aoTranslationsManager;

    private MessageCache messageCache;

    private static final Logger LOG = Logger.getLogger(AbstractCachingTranslationManager.class);

    public AbstractCachingTranslationManager(AbstractAoTranslationsManager aoTranslationsManager) {
        this.aoTranslationsManager = aoTranslationsManager;
        messageCache = new MessageCache();
    }

    @Override
    public void saveTranslation(String locale, String key, String translation) {
        String sanitizedTranslation = MessageTransformUtil.sanitize(translation);
        String finalString = MessageTransformUtil.unquoteMoustaches(sanitizedTranslation);

        if (cacheInitialized) {
            messageCache.save(locale, key, finalString);
            aoTranslationsManager.saveTranslation(locale, key, finalString);
        } else {
            LOG.warn("Cache not initialized. Not saving translation of key " + key + " (" + translation + ") in locale " + locale);
        }
    }

    @Override
    public String getTranslation(String locale, String key) {
        if (cacheInitialized) {
            return messageCache.get(locale, key);
        }
        return null;
    }

    @Override
    public String deleteTranslation(String locale, String key) throws Exception {
        if (cacheInitialized) {
            messageCache.delete(locale, key);
            return aoTranslationsManager.deleteTranslation(locale, key);
        }
        String message = "Cache not initialized. Not deleting translation of key " + key + " in locale " + locale;
        LOG.warn(message);

        throw new Exception(message);
    }

    @Override
    public void deleteAllTranslations(String locale) {
        if (cacheInitialized) {
            messageCache.delete(locale);
            aoTranslationsManager.deleteAllTranslations(locale);
            return;
        }
        String message = "Cache not initialized. Not deleting translations in locale " + locale;
        LOG.warn(message);
    }

    @Override
    public boolean isAnyTranslation(String locale) {
        return aoTranslationsManager.isAnyTranslation(locale);
    }

    @Override
    public Map<String, String> getAllTranslations(String locale) {
        return messageCache.getMessages(locale);
    }

    private void initializeCache() {
        LOG.info("Initializing translation cache from ActiveObjects...");
        try {
            TranslationsEntity[] allTranslations = aoTranslationsManager.getAllTranslations();
            for (TranslationsEntity translation : allTranslations) {
                LOG.debug("Loading to cache: " + translation.getLocale() + " " + translation.getKey() + ": " + translation.getTranslation());
                messageCache.save(translation.getLocale(), translation.getKey(), translation.getTranslation());
            }
            cacheInitialized = true;
            LOG.info("Initialized translation cache from ActiveObjects");
        } catch (Exception e) {
            if (++initCounter < 3) {
                LOG.warn("cache initialization failed: " + e.getMessage() +", retrying", e);
                initializeCache();
            } else {
                initCounter = 0;
                cacheLoadStarted = false;
            }
        }
   }

    protected abstract void runTask(Callable<Serializable> task, String message);

    protected void loadAoToCache() {
        runTask(
            new Callable<Serializable>() {
               @Override
               public Serializable call() throws Exception {
                   initializeCache();
                   return null;
               }
            },
            "MessageCache initialization"
        );
    }

}
