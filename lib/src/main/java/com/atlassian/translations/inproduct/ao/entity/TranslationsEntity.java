package com.atlassian.translations.inproduct.ao.entity;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.StringLength;

/**
 */

// todo remove preload

@Preload
public interface TranslationsEntity extends Entity {

	String getTranslation();

    @StringLength(value= StringLength.UNLIMITED)
	void setTranslation(String translation);
	
	String getKey();
	
	void setKey(String key);

	String getLocale();

	void setLocale(String key);

}
