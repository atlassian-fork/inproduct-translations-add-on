package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.0
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
public class OriginalMessageRepresentation {
	@XmlElement
	private String message;

	public OriginalMessageRepresentation() {
	}

	public OriginalMessageRepresentation(String message) {
		this.message = message;
	}
}
