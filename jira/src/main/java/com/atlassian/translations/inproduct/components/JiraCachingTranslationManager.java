package com.atlassian.translations.inproduct.components;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.task.TaskContext;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.translations.inproduct.ao.AoTranslationsManager;

import java.io.Serializable;
import java.util.concurrent.Callable;

/**
 * User: kalamon
 * Date: 10.07.12
 * Time: 15:08
 */
public class JiraCachingTranslationManager extends AbstractCachingTranslationManager implements LifecycleAware {
    private final TaskManager taskManager;

    public JiraCachingTranslationManager(ActiveObjects ao, TaskManager taskManager) {
        super(new AoTranslationsManager(ao));
        this.taskManager = taskManager;
    }

    @Override
    protected void runTask(Callable<Serializable> task, String message) {
        taskManager.submitTask(task, message, (TaskContext) aLong -> null);
    }

    @Override
    public void onStart() {
        loadAoToCache();
    }

    @Override
    public void onStop() {
    }
}
