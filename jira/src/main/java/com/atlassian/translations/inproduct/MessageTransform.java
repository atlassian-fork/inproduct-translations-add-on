package com.atlassian.translations.inproduct;

import com.atlassian.jira.plugin.language.TranslationTransform;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.translations.inproduct.components.MessageKeyGuesser;
import com.atlassian.translations.inproduct.components.TranslationManager;
import webwork.action.ActionContext;

import javax.servlet.http.HttpServletRequest;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.0
 */
public class MessageTransform extends AbstractMessageTransform implements TranslationTransform {
	public MessageTransform(TranslationManager translationManager, MessageKeyGuesser guesser) {
        super(translationManager, guesser);
	}

    @Override
    protected HttpServletRequest getCurrentRequest() {
        HttpServletRequest request = ActionContext.getRequest();
        if (request == null) {
            request = ExecutingHttpRequest.get();
        }
        return request;
    }
}
