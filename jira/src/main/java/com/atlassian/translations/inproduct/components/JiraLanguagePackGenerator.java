package com.atlassian.translations.inproduct.components;

import com.atlassian.jira.template.TemplateSources;
import com.atlassian.jira.template.VelocityTemplatingEngine;

import java.util.Map;

/**
 * User: kalamon
 * Date: 12.04.13
 * Time: 12:06
 */
public class JiraLanguagePackGenerator extends AbstractLanguagePackGenerator {
    private final VelocityTemplatingEngine velocity;

    public JiraLanguagePackGenerator(VelocityTemplatingEngine velocity, TranslationManager translationManager) {
        super(translationManager);
        this.velocity = velocity;
    }

    @Override
    protected String getProductName() {
        return "JIRA";
    }

    @Override
    protected String getWebActionSupportPath() {
        return "com/atlassian/jira/web/action/JiraWebActionSupport";
    }

    @Override
    protected String renderLanguagePackXml(Map<String, Object> context) {
        return velocity.render(TemplateSources.file("/templates/vm/admin/languagepack.vm")).applying(context).asPlainText();
    }
}
