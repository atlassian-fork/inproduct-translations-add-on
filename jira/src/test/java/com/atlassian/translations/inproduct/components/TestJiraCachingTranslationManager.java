package com.atlassian.translations.inproduct.components;

import com.atlassian.jira.task.TaskContext;
import com.atlassian.jira.task.TaskManager;
import org.apache.commons.io.output.NullOutputStream;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.ObjectOutputStream;

import static org.junit.Assert.*;


public class TestJiraCachingTranslationManager {
    @Test
    public void shouldPassSerializableTaskContext() throws Exception {
        final TaskManager taskManager = Mockito.mock(TaskManager.class);
        final JiraCachingTranslationManager manager = new JiraCachingTranslationManager(null, taskManager);
        final ArgumentCaptor<TaskContext> captor = ArgumentCaptor.forClass(TaskContext.class);
        Mockito.when(taskManager.submitTask(Mockito.any(), Mockito.any(), captor.capture())).thenReturn(null);

        manager.runTask(null, null);

        final TaskContext context = captor.getValue();
        assertTrue(isSerializable(context));
    }

    private boolean isSerializable(Object o) {
        if (o == null) {
            return true;
        }

        try (ObjectOutputStream os = new ObjectOutputStream(NullOutputStream.NULL_OUTPUT_STREAM)) {
            os.writeObject(o);
            return true;
        } catch (IOException e) {
            System.out.println(e);
            return false;
        }
    }
}